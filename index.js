db.users.insertOne({
  "username": "dahyunkim",
  "password": "once1234"
})


db.users.insertOne({
  "username": "Mckyle Luis",
  "password": "Kyle1234"
})

db.users.insertMany([
  {
    "username": "pablo123",
    "password": "123paul"
  },
  {
    "username": "pedro99",
    "password": "iampeter99"
  }

])

// commands for creating documents:
// db.collection.insertOne({
"field1": "value",
  "field2": "value" 
 })

db.collection.insertMany([
  {
    "field1": "valueA",
    "field2": "valueA"
  },
  {
    "field1": "valueB",
    "field2": "valueB"
  }
])


db.products.find()

db.users.find({ "username": "pedro99" })

db.cars.findOne({}) - to find only one

db.users.updateOne({ username: "pablo123" }, { $set: { isadmin: true } })

// updateMany() - allows us to update all documents that matches that criteria

// db.getCollection('users').find({})


db.users.updateMany({}, { $set: { isAdmin: true } })

db.cars.deleteOne({ brand: "toyota" })


